package weoptim.repository.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import weoptim.repository.ValueRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


@SpringBootTest
@RunWith(SpringRunner.class)
public class ValueRepositoryImplTest {

    static Jedis jedis;
    static Map<String,String> testVentes = new HashMap<>(20);
    static Map<String,String> testStock = new HashMap<>(20);
    static Map<String,String> testCommandes = new HashMap<>(20);
    static Map<String,String> testRetours = new HashMap<>(20);

    @Autowired
    ValueRepository repo;

    @Before
    public void setUp() throws Exception {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        JedisPool pool = new JedisPool(poolConfig,"localhost",6379,150000);
        jedis = pool.getResource();

        for(int i=0;i<20;i++) testVentes.put(""+i,""+i);
        for(int i=0;i<20;i++) testStock.put(""+i,""+i);
        for(int i=0;i<20;i++) testCommandes.put(""+i,""+i);
        for(int i=0;i<20;i++) testRetours.put(""+i,""+i);
    }

    @After
    public void tearDown() throws Exception {
        jedis.keys("product*").forEach(key -> jedis.del(key));
        jedis.close();
    }

    @Test
    public void findValue() {
        jedis.hmset("product5:ventes",testVentes);
        String value = repo.findValue("product5","ventes","10");
        if(value == null) assertTrue(false);
        assertTrue(value.equalsIgnoreCase("10"));
    }

    @Test
    public void findValues() {
        jedis.hmset("product6:ventes",testVentes);

        ArrayList<String> keys = new ArrayList<>();
        keys.add("1");
        keys.add("2");
        keys.add("3");

        Map<String,String> expected = new HashMap<>();
        expected.put("1","1");
        expected.put("2","2");
        expected.put("3","3");

        Map<String,String> values = repo.findValues("product6","ventes",keys);
        assertThat(values, is(expected));
    }

    @Test
    public void saveValue() {
        jedis.hmset("product10:ventes",testVentes);
        repo.saveValue("product10","ventes","10","100");
        String val = jedis.hget("product10:ventes", "10");
        assertEquals("100",val);
    }

    @Test
    public void updateValue() {
    }

    @Test
    public void removeValue() {
        jedis.hmset("product13:ventes",testVentes);
        repo.removeValue("product13","ventes","1");
        String actual = jedis.hget("product13:ventes","1");
        assertEquals(null,actual);
    }

    @Test
    public void removeValues() {
        jedis.hmset("product14:ventes",testVentes);
        Map<String,String> actual = new HashMap<>();
        actual.put("2",jedis.hget("product14:ventes","2"));
        actual.put("3",jedis.hget("product14:ventes","3"));

        ArrayList<String> keys = new ArrayList<>();
        keys.add("2"); keys.add("3");
        repo.removeValues("product14","ventes",keys);

        Map<String,String> expected = new HashMap<>();
        expected.put("2",null);
        expected.put("3",null);

        assertTrue(actual.get("2") != expected.get("2") && actual.get("3") != expected.get("3")  );
    }


}