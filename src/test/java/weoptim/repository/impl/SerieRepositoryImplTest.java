package weoptim.repository.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import weoptim.domain.Serie;
import weoptim.repository.SerieRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


@SpringBootTest
@RunWith(SpringRunner.class)
public class SerieRepositoryImplTest {

    static Jedis jedis;
    static Map<String,String> testVentes = new HashMap<>(20);
    static Map<String,String> testStock = new HashMap<>(20);
    static Map<String,String> testCommandes = new HashMap<>(20);
    static Map<String,String> testRetours = new HashMap<>(20);

    @Autowired
    SerieRepository repo;

    @Before
    public void setUp() throws Exception {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        JedisPool pool = new JedisPool(poolConfig,"localhost",6379,150000);
        jedis = pool.getResource();

        for(int i=0;i<20;i++) testVentes.put(""+i,""+i);
        for(int i=0;i<20;i++) testStock.put(""+i,""+i);
        for(int i=0;i<20;i++) testCommandes.put(""+i,""+i);
        for(int i=0;i<20;i++) testRetours.put(""+i,""+i);
    }

    @After
    public void tearDown() throws Exception {
        jedis.keys("product*").forEach(key -> jedis.del(key));
        jedis.close();
    }

    @Test
    public void findSerie() {
        jedis.hmset("product2:ventes",testVentes);
        Serie serie = repo.findSerie("product2","ventes");

        assertThat(serie.getValues(),is(testVentes));
    }

    @Test
    public void findSeries() {
        ArrayList<String> ids = new ArrayList<String>();
        ids.add("product3");
        ids.add("product4");

        jedis.hmset("product3:ventes",testVentes);
        jedis.hmset("product3:commandes",testCommandes);

        jedis.hmset("product4:ventes",testVentes);
        jedis.hmset("product4:retours",testRetours);

        Map<String,Serie> series = repo.findSeries(ids,"ventes");
        assertTrue(series.size() == 2 && series.get("product3").getValues().size() == 20 );
    }

    @Test
    public void saveSeries() {
        jedis.hmset("product8:ventes",testVentes);

        List<Serie> series = new ArrayList<>();
        series.add(new Serie("product8","stock",testStock));
        series.add(new Serie("product8","ventes",testVentes));

        List savedSeries = repo.saveSeries("product8",series);

         assertTrue(savedSeries.size() == 2);
    }

    @Test
    public void saveSerie() {
        jedis.hmset("product9:ventes",testVentes);

        Map<String,String> testVentes2 = new HashMap<>();
        testVentes2.put("25","25");


        repo.saveSerie("product9",new Serie("product9","ventes",testVentes2));

        Map<String,String> expected = jedis.hgetAll("product9:ventes");
        assertEquals(21 , expected.size());
    }

    @Test
    public void updateSerie() {
    }

    @Test
    public void updateSerieNew() {
    }

    @Test
    public void removeSerie() {
        jedis.hmset("product12:ventes",testVentes);
        repo.removeSerie("product12","ventes");
        Map<String,String> serie = jedis.hgetAll("product12:ventes");
        assertTrue(serie.size() == 0);
    }

    @Test
    public void exists() {
    }
}