package weoptim.service.impl;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import weoptim.domain.Serie;
import weoptim.repository.ValueRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ValueServiceImplTest {

    @Mock
    ValueRepository repo;

    @InjectMocks
    ValueServiceImpl valueService;

    static Serie serie;

    @BeforeClass
    public static void setUp() throws Exception {
        Map<String,String> testVentes = new HashMap<>(20);
        for(int i=0;i<20;i++) testVentes.put(""+i,""+i);

        Serie serie = new Serie("product0","ventes");
        serie.setValues(testVentes);
    }

    @AfterClass
    public static void tearDown() throws Exception {

    }


    @Test
    public void findOne() {
        String expected = "10";

        when(repo.findValue("product0","ventes","10")).thenReturn("10");
        when(repo.exists("product0","ventes","10")).thenReturn(true);

        assertEquals(expected,valueService.findOne("product0","ventes","10"));
    }

    @Test
    public void findAll() {
        ArrayList<String> keys = new ArrayList<>();
        keys.add("5");
        keys.add("6");

        Map<String,String> expected = new HashMap<>();
        expected.put("5","5");
        expected.put("6","6");

        when(repo.findValues("product0","ventes",keys)).thenReturn(expected);

        assertEquals(expected,valueService.findValues("product0","ventes",keys));
    }

    @Test
    public void save() {
        when(repo.saveValue("product0","ventes", "10", "10")).thenReturn("10");
        String expected = valueService.save("product0","ventes","10","10");
        assertEquals(expected,"10");
    }

    @Test
    public void update() {
    }

    @Test
    public void remove() {
        when(repo.removeValue("product0","ventes","10")).thenReturn(true);
        when(repo.exists("product0","ventes","10")).thenReturn(true);
        assertTrue(valueService.remove("product0","ventes","10"));
    }

    @Test
    public void removeAll() {
        List<String> keys = new ArrayList<>();
        keys.add("5");
        keys.add("6");

        when(repo.removeValues("product0","ventes",keys)).thenReturn(true);
        assertTrue(valueService.removeValues("product0","ventes",keys));
    }
}