package weoptim.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import weoptim.domain.Serie;
import weoptim.service.OperationService;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class OperationServiceImplTest {

    Serie serie1;
    Serie serie2;

    @Autowired
    OperationService operationService;

    @Before
    public void setUp() throws Exception {
        Map<String,String> values = new HashMap<String,String>();
        values.put("1", "10");
        values.put("2", "20");
        values.put("3", "30");
        serie1 = new Serie("ventes","X",values);

        Map<String,String> values_1 = new HashMap<String,String>();
        values_1.put("2", "10");
        values_1.put("4", "20");
        values_1.put("6", "0");
        serie2 = new Serie("ventes","X2",values_1);
    }

    @Test
    public void sum() {
        Map<String,String> actual = operationService.sum(serie1,serie2);

        Map<String,String> expected = new HashMap<String,String>();
        expected.put("1", "10");
        expected.put("2", "30.0");
        expected.put("3", "30");
        expected.put("4", "20");
        expected.put("6", "0");
        assertEquals(expected,actual);
    }

    @Test
    public void difference() {
        Map<String,String> actual = operationService.difference(serie1,serie2);

        Map<String,String> expected = new HashMap<String,String>();
        expected.put("1", "10");
        expected.put("2", "10.0");
        expected.put("3", "30");
        expected.put("4", "-20.0");
        expected.put("6", "-0.0");
        assertEquals(expected,actual);
    }

    @Test
    public void mutiplication() {
        Map<String,String> actual = operationService.multiplication(serie1,serie2);

        Map<String,String> expected = new HashMap<String,String>();
        expected.put("1", "0");
        expected.put("2", "200");
        expected.put("3", "0");
        expected.put("4", "0");
        expected.put("6", "0");
        assertEquals(expected,actual);
    }

    @Test
    public void division() {
        Map<String,String> actual = operationService.division(serie1,serie2);

        Map<String,String> expected = new HashMap<String,String>();
        expected.put("2", "2.00");
        assertEquals(expected,actual);

    }

    @Test
    public void decalage() {

        Map<String,String> values = new HashMap<String,String>();
        values.put("1", "15");
        values.put("2", "20");
        values.put("3", "10");
        values.put("4", "30");
        values.put("5", "24");
        Serie serie3 = new Serie("ventes","C",values);

        Map<String,String> values_decPos = new HashMap<String,String>();
        values_decPos.put("1", "10");
        values_decPos.put("2", "30");
        values_decPos.put("3", "24");

        Map<String,String> values_decNeg = new HashMap<String,String>();
        values_decNeg.put("3", "15");
        values_decNeg.put("4", "20");
        values_decNeg.put("5", "10");

        Map<String,String> actual = operationService.decalage(serie3,2);
        Map<String,String> actual2 = operationService.decalage(serie3, -2);

        assertTrue(actual.equals(values_decPos) && actual2.equals(values_decNeg));
    }
}