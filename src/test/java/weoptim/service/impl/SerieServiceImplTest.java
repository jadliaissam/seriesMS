package weoptim.service.impl;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import weoptim.domain.Serie;
import weoptim.repository.SerieRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class SerieServiceImplTest {

    @Mock
    SerieRepository repo;

    @InjectMocks
    SerieServiceImpl serieService;

    static Serie serie;

    @BeforeClass
    public static void setUp() throws Exception {
        Map<String,String> testVentes = new HashMap<>(20);
        for(int i=0;i<20;i++) testVentes.put(""+i,""+i);
        serie = new Serie("product","ventes");
        serie.setValues(testVentes);
    }

    @AfterClass
    public static void tearDown() throws Exception {

    }

    @Test
    public void findOne()   {
        when(repo.findSerie("product1","ventes")).thenReturn(serie);
        when(repo.exists("product1","ventes")).thenReturn(true);
        assertEquals(serieService.findOne("product1","ventes"),serie);
    }

    @Test
    public void findAll() {
        ArrayList<String> ids = new ArrayList<>();
        Map<String,Serie> series = new HashMap<>();

        ids.add("product1");
        ids.add("product2");

        series.put("product1",new Serie("product1","ventes",serie.getValues()));
        series.put("product2",new Serie("product2","ventes",serie.getValues()));

        when(repo.findSeries(ids,"ventes")).thenReturn(series);
        assertEquals(series,serieService.findByseriesForProductListBySerieName(ids,"ventes"));
    }

    @Test
    public void saveAll() {
        List<Serie> series = new ArrayList<>();
        Serie ventes = serie;
        ventes.setName("ventes_2");
        series.add(ventes);
        series.add(serie);

        when(repo.saveSeries("product",series)).thenReturn(series);
        assertEquals(serieService.saveListSeries("product",series),series);
    }

    @Test
    public void save() {
        when(repo.saveSerie("product1", serie)).thenReturn(serie);
        assertEquals(serieService.save(serie),serie);
    }

    @Test
    public void update() {
        when(repo.updateSerie("product1", serie)).thenReturn(serie);
        when(repo.exists("product1","ventes")).thenReturn(true);
        assertEquals(serieService.update(serie),serie);
    }

    @Test
    public void updateNew() {
        when(repo.updateSerieNew("product1", serie)).thenReturn(serie);
        when(repo.exists("product1","ventes")).thenReturn(true);
        assertEquals(serieService.updateNew(serie),serie);
    }

    @Test
    public void remove() {
        when(repo.removeSerie("product1","ventes")).thenReturn(true);
        when(repo.exists("product1","ventes")).thenReturn(true);
        assertTrue(serieService.remove("product1","ventes"));
    }
}