package weoptim.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Serie {

    String name;
    String product;
    Map<String,String> values;

    public Serie() {
        this.values = new HashMap<String,String>();
    }

    public Serie(String product, String name) {
        this.name = name;
        this.product = product;
        this.values = new HashMap<String,String>();
    }

    public Serie(String product, String name, Map<String, String> values) {
        this.name = name;
        this.product = product;
        this.values = values;
    }

    public void setValues(Map<String, String> values){
        this.values = values;
    }

    public Map<String, String> getValues(){
        return this.values;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addValue(String key, String val) {
        values.put(key,val);
    }

    public void getValue(String key) {
        values.get(key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Serie serie = (Serie) o;
        return Objects.equals(name, serie.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, values);
    }
}
