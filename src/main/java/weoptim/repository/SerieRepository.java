package weoptim.repository;

import weoptim.domain.Serie;

import java.util.List;
import java.util.Map;

public interface SerieRepository {
    public Serie findSerie(String idProduct, String serie);
    public Map<String,Serie> findSeries(List<String> ids, String serie);
    public List<Serie> saveSeries(String idProduct, List<Serie> series);
    public Serie saveSerie(String idProduct, Serie serie);
    public Serie updateSerie(String idProduct, Serie serie);
    public Serie updateSerieNew(String idProduct, Serie serie);
    public boolean removeSerie(String idProduct, String serie);

    public int massInsert(List<Serie> series);
    public boolean exists(String idProduct, String serie);

}
