package weoptim.repository.impl;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;
import weoptim.configuration.RedisProperty;
import weoptim.domain.Serie;
import weoptim.exception.ErrorException;
import weoptim.repository.SerieRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/*
 *This repository gathers all operations about series management with Jedis and Redis
 * Jedis is used to connect with Redis Server and it is used with a Connection Pool
 */

@Repository
public class SerieRepositoryImpl implements SerieRepository {

    static final Logger logger = Logger.getLogger(SerieRepository.class.getName());
    String errorMsg = "An error has occured and the requested operation was not Done ! Please Check Logs";

    //Wiring an instance of the connections Pool
    @Autowired
    JedisPool pool;

    //Wiring instance of RedisProperty which contains all Jedis params (defined in application.yaml) for connectionng to Redis
    @Autowired
    RedisProperty redisProperty ;

    /**
     * Method to get ONE time serie
     * @return Serie Object
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     **/
    @Override
    public Serie findSerie(String idProduct, String serieName) {
        try {
            Jedis jedis = pool.getResource();
            Serie serie = new Serie(idProduct, serieName,jedis.hgetAll(idProduct+":"+serieName));
            jedis.close();
            return serie;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to get a list of series with the same Name and belonging to differents products
     * @return Map(key = ID Product , value = Serie Object for this ID Product)
     * @param idsList : List of Product IDs
     * @param serieName : Time serie Name
     **/
    @Override
    public Map<String,Serie> findSeries(List<String> idsList, String serieName) {
        int bSize = redisProperty.getBatchSize();
        int batchSize = (idsList.size() < bSize) ? bSize : idsList.size() / bSize;
        Map<String, Response> responses = new HashMap<>(idsList.size());
        Map<String, Serie> allResults = new HashMap<>(idsList.size());
        try {
            //Splitting the IDs list to smaller lists based on batch-size parameter
            final List<List<String>> batch = Lists.partition(idsList, batchSize);
            // pipeline all requests for a batch in one go
            batch.stream().forEach( iDs -> {
                Jedis jedis = pool.getResource();
                Pipeline p = jedis.pipelined();
                responses.putAll(iDs.stream().collect(Collectors.toConcurrentMap(
                        id -> String.valueOf(id),
                        id -> p.hgetAll(id + ":" + serieName))));
                // process the pipeline
                p.sync();
                // close the resource and return it to the pool
                jedis.close();
            });
            // Gather responses from server
            Map<String,Serie> results = responses.entrySet().parallelStream().collect(Collectors.toConcurrentMap(
                    entry -> entry.getKey(),
                    entry -> new Serie(entry.getKey(),serieName, (Map) entry.getValue().get())
            ));
            allResults.putAll(results);
            return allResults;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to save a list of time series to Redis
     * @return List of saved Series
     * @param idProduct : Product ID
     * @param series : Liste of series to save
     **/
    @Override
    public List<Serie> saveSeries(String idProduct, List<Serie> series) {
        try {
            Jedis jedis = pool.getResource();
            series.stream().forEach(serie -> jedis.hmset(idProduct + ":" + serie.getName(), serie.getValues()));
            jedis.close();
            return series;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to save ONE time serie
     * @return Serie Object
     * @param idProduct : Product ID
     * @param serie : Time serie Object containing values.
     **/
    @Override
    public Serie saveSerie(String idProduct,Serie serie) {
        try {
            Jedis jedis = pool.getResource();
            jedis.hmset(idProduct + ":" + serie.getName(), serie.getValues());
            jedis.close();
            return serie;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to update ONE time serie with nwe values while keeping old ones
     * @return Serie Object
     * @param idProduct : Product ID
     * @param serie : Time serie Object containing values.
     **/
    @Override
    public Serie updateSerie(String idProduct, Serie serie) {
        try {
            Jedis jedis = pool.getResource();
            jedis.hmset(idProduct + ":" + serie.getName(), serie.getValues());
            jedis.close();
            return serie;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage()+" Error");
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to update ONE time serie with nwe values while removing old ones
     * @return Serie Object (updated)
     * @param idProduct : Product ID
     * @param serie : Time serie Object containing values.
     **/
    @Override
    public Serie updateSerieNew(String idProduct, Serie serie) {
        try {
            Jedis jedis = pool.getResource();
            jedis.del(idProduct + ":" + serie.getName());
            jedis.hmset(idProduct + ":" + serie.getName(), serie.getValues());
            jedis.close();
            return serie;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to remove ONE time serie
     * @return operation success (true/false) where "count" is the number of records deleted
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     **/
    @Override
    public boolean removeSerie(String idProduct, String serieName) {
        try {
            long count = 0;
            Jedis jedis = pool.getResource();
            count += jedis.del(idProduct + ":" + serieName);
            jedis.close();
            return (count > 0 ) ;

        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to check if a time-serie exists or not
     * @return existance status as boolean (true/false)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name.
     **/
    @Override
    public boolean exists(String idProduct, String serieName) {
        try {
            Jedis jedis = pool.getResource();
            Map<String,String> serieValues = jedis.hgetAll(idProduct+":"+serieName);
            // if the serie does not exist the Map is empty
            if(serieValues.isEmpty()) return false;
            jedis.close();
            return true;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to mass insert time series using list of series coming from a flat file
     * @return number of series inserted in Redis
     * @param series : List of Series Objects
     **/
    @Override
    public int massInsert(List<Serie> series) {
        int batchSize = 0;
        // change batch-size based on input size to improve speed
        if(series.size() < 1000) batchSize = 5;
        else if(series.size() < 10000) batchSize = 20;
        else if(series.size() < 100000) batchSize = 100;
        else if(series.size() < 1000000) batchSize = 1000;
        else batchSize = 10000;
        try {
            // split the list based on batch-size
            final List<List<Serie>> batch = Lists.partition(series, batchSize);
            //process every batch in one pipeline
            batch.stream().forEach(seriesBatch -> {
                Jedis jedis = pool.getResource();
                Pipeline p = jedis.pipelined();
                seriesBatch.forEach(serie -> jedis.hmset(serie.getProduct()+":"+serie.getName(),serie.getValues()));
                p.sync();
                jedis.close();
            });
            return series.size();
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }
}
