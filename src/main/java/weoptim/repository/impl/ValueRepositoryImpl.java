package weoptim.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import weoptim.configuration.RedisProperty;
import weoptim.exception.ErrorException;
import weoptim.repository.ValueRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 *This repository gathers all operations about time series values management with Jedis and Redis
 * Jedis is used to connect with Redis Server and it is used with a Connection Pool
 */
@Repository
public class ValueRepositoryImpl implements ValueRepository {

    static final Logger logger = Logger.getLogger(ValueRepository.class.getName());
    String errorMsg = "An error has occured and the requested operation was not Done ! Please Check Logs";

    //Wiring an instance of the connections Pool
    @Autowired
    JedisPool pool;

    //Wiring instance of RedisProperty which contains all Jedis params (defined in application.yaml) for connecting to Redis
    @Autowired
    RedisProperty redisProperty ;

    /**
     * Method to get ONE value
     * @return The Value (String)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param period : The chosen period
     **/
    @Override
    public String findValue(String idProduct, String serieName, String period) {
        try {
            Jedis jedis = pool.getResource();
            String value = jedis.hget(idProduct + ":" + serieName, period);
            jedis.close();
            return value;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to get Multiple values
     * @return Map of selected values (key=period,value=Value)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param periods : List of chosen periods
     **/
    @Override
    public Map<String, String> findValues(String idProduct, String serieName, List<String> periods) {
        Map<String,String> values = new HashMap<>(periods.size());
        try {
            Jedis jedis = pool.getResource();
            periods.stream().forEach(period -> values.put(period,jedis.hget(idProduct+":"+serieName,period)));
            jedis.close();
            return values;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to save ONE value
     * @return The Saved Value (String)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param period : The chosen period
     * @param value : The value to save
     **/
    @Override
    public String saveValue(String idProduct, String serieName, String period, String value) {
        try {
            Jedis jedis = pool.getResource();
            jedis.hset(idProduct + ":" + serieName, period, value);
            jedis.close();
            return value;
        } catch(Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to update ONE value. it calls the saveValue() method since Redis does not have a specific update method.
     * @return The Updated Value (String)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param period : The chosen period
     * @param value : The value to update
     **/
    @Override
    public String updateValue(String idProduct, String serieName, String period, String value) {
        return saveValue(idProduct,serieName,period,value);
    }

    /**
     * Method to remove ONE value
     * @return The Removed Value (String)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param period : The chosen period
     **/
    @Override
    public boolean removeValue(String idProduct, String serieName, String period) {
        try {
            long count = 0;
            Jedis jedis = pool.getResource();
            count += jedis.hdel(idProduct + ":" + serieName, period);
            jedis.close();
            return count > 0 ;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to remove Multiple values
     * @return operation success status (true/false)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param periods : List of chosen periods to remove with their values
     **/
    @Override
    public boolean removeValues(String idProduct, String serieName, List<String> periods) {
        try {
            Jedis jedis = pool.getResource();
            periods.stream().forEach(key -> jedis.hdel(idProduct+":"+serieName,key));
            jedis.close();
            return true;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

    /**
     * Method to check if a value exists by checking if the period exists
     * @return operation success status (true/false)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param period : The chosen period
     **/
    @Override
    public boolean exists(String idProduct, String serieName, String period) {
        try {
            Jedis jedis = pool.getResource();
            String value = jedis.hget(idProduct+":"+serieName,period);
            // if the value is null, the period does not exist in the serie
            jedis.close();
            return value != null;
        } catch(Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
            throw new ErrorException(errorMsg);
        }
    }

}
