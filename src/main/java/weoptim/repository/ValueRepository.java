package weoptim.repository;

import java.util.List;
import java.util.Map;

public interface ValueRepository {

    public String findValue(String idProduct, String serie, String key);
    public Map<String,String> findValues(String idProduct, String serie, List<String> keys);
    public String saveValue(String idProduct, String serie, String key, String val);
    public String updateValue(String idProduct, String serie, String key, String val);

    public boolean removeValue(String idProduct, String serie, String key);
    public boolean removeValues(String idProduct, String serie, List<String> keys);

    public boolean exists(String idProduct, String serie, String val);

}
