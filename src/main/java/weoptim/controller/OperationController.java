package weoptim.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import weoptim.service.OperationService;
import weoptim.service.SerieService;

import java.util.Map;

@RestController
@RequestMapping("/operation")
public class OperationController {

    @Autowired
    OperationService operationService;

    @Autowired
    SerieService serieService;

    @RequestMapping(value = "/sum/{serie1Name}/{idProduct1}/{serie2Name}/{idProduct2}",method = RequestMethod.GET)
    @ApiOperation(value = "Sum two time series whom Names and ID Products are given in Params")
    public Map<String,String> sum(@PathVariable String serie1Name, @PathVariable String idProduct1, @PathVariable String serie2Name, @PathVariable String idProduct2){
        return operationService.sum(serie1Name,idProduct1,serie2Name,idProduct2);
    }
    @RequestMapping(value = "/difference/{serie1Name}/{idProduct1}/{serie2Name}/{idProduct2}",method = RequestMethod.GET)
    @ApiOperation(value = "Calculate the difference between  two time series whom Names and ID Products are given in Params")
    public Map<String,String> minus(@PathVariable String serie1Name, @PathVariable String idProduct1, @PathVariable String serie2Name, @PathVariable String idProduct2){
        return operationService.difference(serie1Name,idProduct1,serie2Name,idProduct2);
    }
    @RequestMapping(value = "/multiplication/{serie1Name}/{idProduct1}/{serie2Name}/{idProduct2}",method = RequestMethod.GET)
    @ApiOperation(value = "Calculate the multiplication between two time series whom Names and ID Products are given in Params")
    public Map<String,String> mult(@PathVariable String serie1Name, @PathVariable String idProduct1, @PathVariable String serie2Name, @PathVariable String idProduct2){
        return operationService.multiplication(serie1Name,idProduct1,serie2Name,idProduct2);
    }
    @RequestMapping(value = "/division/{serie1Name}/{idProduct1}/{serie2Name}/{idProduct2}",method = RequestMethod.GET)
    @ApiOperation(value = "Divide time serie A by a another time serie B.  The time series Names and ID Products are given in Params")
    public Map<String,String> div(@PathVariable String serie1Name, @PathVariable String idProduct1, @PathVariable String serie2Name, @PathVariable String idProduct2){
        return operationService.division(serie1Name,idProduct1,serie2Name,idProduct2);
    }

    @RequestMapping(value = "/shift/{serie1Name}/{idProduct1}/{steps}",method = RequestMethod.GET)
    @ApiOperation(value = "Shift values of a Time serie by a number of steps P to the right (p>0) or the Left (p<0). the time serie name,ID Product and P  are given in Params")
    public Map<String,String> dec(@PathVariable String serie1Name, @PathVariable String idProduct1, @PathVariable String steps){
       int factor = Integer.parseInt(steps);
        return operationService.decalage(serie1Name,idProduct1,factor);
    }

}
