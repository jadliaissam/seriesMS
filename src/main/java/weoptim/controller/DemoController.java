package weoptim.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import weoptim.service.impl.DemoServiceImpl;

@RestController
public class DemoController {
    @Autowired
    DemoServiceImpl demoService;

    @RequestMapping(value = "/demo/generate/{start}/{end}",method = RequestMethod.GET)
    @ApiOperation(value = "Generate a file with desired Demo Data length. IDs of Products start with {start} param and end with the {end} param ")
    public String genererFichier(@PathVariable String start, @PathVariable String end){
        return demoService.generateDataFile(Integer.valueOf(start), Integer.valueOf(end));
    }

    @RequestMapping(value = "/demo/error",method = RequestMethod.GET)
    @ApiOperation(value = "Microservice Error Page")
    public String error(){
        return " This is an Error Page ! you probably tried to access a non-existing URL or an exception was thrown. Check logs !";
    }
}
