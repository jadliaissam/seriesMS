package weoptim.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoController {

    @RequestMapping(value = "/info",method = RequestMethod.GET)
    @ApiOperation(value = "Microservice Info Page")
    public String info(){
        return "RedisMicroService : this is where all Redis Storage and Search is Done ! ";
    }

    @RequestMapping(value = "/error",method = RequestMethod.GET)
    @ApiOperation(value = "Microservice Error Page")
    public String error(){
        return " This is an Error Page ! you probably tried to access a non-existing URL or an exception was thrown. Check logs !";
    }
}
