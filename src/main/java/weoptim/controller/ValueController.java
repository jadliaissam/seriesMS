package weoptim.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import weoptim.service.ValueService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/value")
public class ValueController {

    @Autowired
    ValueService valueService;

    @RequestMapping(value = "/{period}/serie/{serieName}/product/{idProduct}",method = RequestMethod.GET)
    @ApiOperation(value = "Fetch One Value from a product's time serie By Product ID, Serie name and the key")
    public String findValue(@PathVariable String idProduct, @PathVariable String serieName, @PathVariable String period){
        return valueService.findOne(idProduct,serieName,period);
    }

    @RequestMapping(value = "keys/serie/{serieName}/product/{idProduct}",method = RequestMethod.POST)
    @ApiOperation(value = "Fetch values based on List of keys and serie name and Product ID ")
    public Map<String,String> findValues(@PathVariable String idProduct, @PathVariable String serieName, @RequestBody List<String> periods){
        return valueService.findValues(idProduct,serieName,periods);
    }


    @RequestMapping(value = "/serie/{serieName}/product/{idProduct}",method = RequestMethod.POST)
    @ApiOperation(value = "Saving One value belonging to the key time in the designed serie for the specified Product")
    public String saveValue(@PathVariable String idProduct, @PathVariable String serieName, @RequestParam String period, @RequestParam String value ){
        return   valueService.save(idProduct,serieName,period,value);
    }


    @RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/{period}/serie/{serieName}/product/{idProduct}",method = RequestMethod.PUT)
    @ApiOperation(value = "Updating The value by ID Product, Serie nae and period key")
    public String updateValue(@PathVariable String idProduct, @PathVariable String serieName, @PathVariable String period, @RequestParam String value){
        return valueService.update(idProduct,serieName,period,value);
    }

    @RequestMapping(value = "/keys/serie/{serieName}/product/{idProduct}",method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete Values from a product's time serie By Product ID, Serie name and the period List")
    public ResponseEntity deleteValues(@PathVariable String idProduct, @PathVariable String serieName, @RequestBody List<String> periods){
        boolean result = valueService.removeValues(idProduct,serieName,periods);
        if (result) return new ResponseEntity(HttpStatus.OK);
        else return new ResponseEntity(HttpStatus.EXPECTATION_FAILED);
    }

    @RequestMapping(value = "/{period}/serie/{serieName}/product/{idProduct}",method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete One Value from a product's time serie By Product ID, Serie name and the period")
    public ResponseEntity deleteValue(@PathVariable String idProduct, @PathVariable String serieName, @PathVariable String period){
        boolean result = valueService.remove(idProduct,serieName,period);
        if (result) return new ResponseEntity(HttpStatus.OK);
        else return new ResponseEntity(HttpStatus.EXPECTATION_FAILED);
    }

}
