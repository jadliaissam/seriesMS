package weoptim.configuration;

import org.springframework.beans.factory.annotation.Value;

public class RedisProperty {

   @Value("${datastore.redis.host}")
    private String host;

   @Value("${datastore.redis.port}")
    private int port;

  @Value("${datastore.redis.timeout}")
    private int timeout;

    @Value("${datastore.redis.maxIdle}")
    private int maxIdle;

    @Value("${datastore.redis.maxTotal}")
    private int maxTotal;

    @Value("${datastore.redis.maxWaitMillis}")
    private int maxWaitMillis;

    @Value("${datastore.optimization.batch-size}")
    private int batchSize;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(int maxIdle) {
        this.maxIdle = maxIdle;
    }

    public int getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
    }

    public int getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public void setMaxWaitMillis(int maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }
}