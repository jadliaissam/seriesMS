package weoptim.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisConfig {

    @Autowired
    RedisProperty redisProperty ;

    @Bean
    public JedisPool pool(){
        String host = redisProperty.getHost();
        int port = redisProperty.getPort();
        int timeout = redisProperty.getTimeout();
        int maxIdle = redisProperty.getMaxIdle();
        int maxWaitMillis = redisProperty.getMaxWaitMillis();
        int maxTotal = redisProperty.getMaxTotal();

        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setMaxIdle(maxIdle);
        poolConfig.setTestOnCreate(true);
        poolConfig.setMaxTotal(maxTotal);
        poolConfig.setMaxWaitMillis(maxWaitMillis);

        return new JedisPool(poolConfig,host,port,timeout);
    }

    @Bean
    public RedisProperty redisProperty() { return new RedisProperty(); }

}
