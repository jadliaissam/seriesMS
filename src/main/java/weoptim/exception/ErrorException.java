package weoptim.exception;

public  class ErrorException extends RuntimeException {

    public ErrorException(String message) {
        super(message);
    }

    public ErrorException(String message, Exception cause) {

        super(message, cause);
    }
}
