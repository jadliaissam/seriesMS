package weoptim.exception;

public  class ForbidenException extends RuntimeException {

    public ForbidenException(String message) {
        super(message);
    }

    public ForbidenException(String message, Exception cause) {

        super(message, cause);
    }
}
