package weoptim.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = NotFoundException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected Response handleNotFound(Exception ex, WebRequest request) {
        return new Response(ex.getMessage());
    }

    @ExceptionHandler(value = ForbidenException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.FORBIDDEN)
    protected Response handleForbiden(Exception ex, WebRequest request) {
        return new Response(ex.getMessage());
    }

    @ExceptionHandler(value = ErrorException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected Response handleError(Exception ex, WebRequest request) {
        return new Response(ex.getMessage());
    }
}


