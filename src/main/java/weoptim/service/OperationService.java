package weoptim.service;

import org.springframework.stereotype.Service;
import weoptim.domain.Serie;

import java.util.Map;

@Service
public interface OperationService {
    public Map<String,String> sum(Serie serie1, Serie serie2);
    public Map<String,String> difference(Serie serie1, Serie serie2);
    public Map<String,String> multiplication(Serie serie1, Serie serie2);
    public Map<String,String> division(Serie serie1, Serie serie2);
    public Map<String,String> decalage(Serie serie, int steps);

    public Map<String,String> sum(String iDserie1, String idProduct1, String iDserie2, String idProduct2);
    public Map<String,String> difference(String iDserie1, String idProduct1, String iDserie2, String idProduct2);
    public Map<String,String> multiplication(String iDserie1, String idProduct1, String iDserie2, String idProduct2);
    public Map<String,String> division(String iDserie1, String idProduct1, String iDserie2, String idProduct2);
    public Map<String,String> decalage(String iDserie1, String idProduct1, int steps);
}
