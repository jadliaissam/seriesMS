package weoptim.service;

import java.util.List;
import java.util.Map;

public interface ValueService {

    public String findOne(String idProduct, String serie, String key);
    public Map<String,String> findValues(String idProduct, String serie, List<String> keys);
    public String save(String idProduct, String serie, String key, String val);
    public String update(String idProduct, String serie, String key, String val);
    public boolean remove(String idProduct, String serie, String key);
    public boolean removeValues(String idProduct, String serie, List<String> keys);
}
