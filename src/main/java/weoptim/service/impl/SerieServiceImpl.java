package weoptim.service.impl;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import weoptim.domain.Serie;
import weoptim.exception.ErrorException;
import weoptim.exception.ForbidenException;
import weoptim.exception.NotFoundException;
import weoptim.repository.SerieRepository;
import weoptim.service.SerieService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 *This Service provide methods for time series management with SeriesRepository
 *
 */
@Service
public class SerieServiceImpl implements SerieService {

    //Wiring an instance of the SerieRepository
    @Autowired
    SerieRepository repo;

    static final Logger logger = Logger.getLogger(SerieRepository.class.getName());
    static final String missingIds = "Missing ID Product and/or Serie Name";
    static final String emptySerie = "Missing Serie values. Serie contains no values";

    /**
     * Method to get ONE time serie
     * @return Serie Object
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     **/
    @Override
    public Serie findOne(String idProduct, String serieName) {
        if (idProduct.isEmpty() || serieName.isEmpty())
            throw new ErrorException(missingIds);
        if (!repo.exists(idProduct, serieName))
            throw new NotFoundException("The Serie  (Name : " + serieName + ". Product #" + idProduct + ") does not exist.");
        return repo.findSerie(idProduct, serieName);
    }

    /**
     * Method to get a list of series with the same Name and belonging to differents products
     * @return Map(key = ID Product , value = Serie Object for this ID Product)
     * @param ids : List of Product IDs
     * @param serieName : Time serie Name
     **/
    @Override
    public Map<String, Serie> findByseriesForProductListBySerieName(List<String> ids, String serieName) {
        if (ids.isEmpty() || serieName.isEmpty())
            throw new ErrorException(missingIds);
        return repo.findSeries(ids, serieName);
    }

    /**
     * Method to save a list of time series to Redis
     * @return List of saved Series
     * @param idProduct : Product ID
     * @param series : Liste of series to save
     **/
    @Override
    public List<Serie> saveListSeries(String idProduct, List<Serie> series) {
        return repo.saveSeries(idProduct, series);
    }

    /**
     * Method to save ONE time serie
     * @return Serie Object
     * @param serie : Time serie Object containing values.
     **/
    @Override
    public Serie save(Serie serie) {
        String idProduct = serie.getProduct();
        if (serie.getValues().isEmpty())
            throw new ErrorException(emptySerie);
        if (repo.exists(idProduct, serie.getName()))
            throw new ForbidenException("This Serie (Name : " + serie.getName() + ") Exists Already ! remove it First or use Update Route");
        return repo.saveSerie(idProduct, serie);
    }

    /**
     * Method to update ONE time serie with nwe values while keeping old ones
     * @return Serie Object
     * @param serie : Time serie Object containing values.
     **/
    @Override
    public Serie update(Serie serie) {
        String idProduct = serie.getProduct();
        if (serie.getValues().isEmpty())
            throw new ErrorException(emptySerie);
        if (!repo.exists(idProduct, serie.getName()))
            throw new NotFoundException("The Serie (Name:" + serie.getName() + ". Product #" + idProduct + ") does not exist.");
        repo.updateSerie(idProduct, serie);
        return repo.findSerie(idProduct,serie.getName());
    }

    /**
     * Method to update ONE time serie with nwe values while removing old ones
     * @return Serie Object (updated)
     * @param serie : Time serie Object containing values.
     **/
    @Override
    public Serie updateNew(Serie serie) {
        String idProduct = serie.getProduct();
        if (serie.getValues().isEmpty())
            throw new ErrorException(emptySerie);
        if (!repo.exists(idProduct, serie.getName()))
            throw new NotFoundException("The Serie (Name :" + serie.getName() + ". Product #" + idProduct + ") does not exist.");
        return repo.updateSerieNew(idProduct, serie);
    }

    /**
     * Method to remove ONE time serie
     * @return operation success (true/false) where "count" is the number of records deleted
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     **/
    @Override
    public boolean remove(String idProduct, String serieName) {
        if (idProduct.isEmpty() || serieName.isEmpty())
            throw new ErrorException(missingIds);
        if (!repo.exists(idProduct, serieName))
            throw new NotFoundException("The Serie  (Name:" + serieName + ". Product #" + idProduct + ") does not exist.");
        return repo.removeSerie(idProduct, serieName);
    }

    /**
     * Method to mass import time series using flat file
     * @return the number of records inserted
     * @param file : MultiPartFile uploaded containing series informations
     **/
    @Override
    public int importFile(MultipartFile file) {
        byte[] input = new byte[0];
        try {
            // getting the uploaded file and saving it to disk
            input = file.getBytes();
            File f = new File("temp");
            FileUtils.writeByteArrayToFile(f, input);
            // parse file and get List of Lines (each line is a time serie)
            List<String> lines = Files.readAllLines(Paths.get(f.getAbsolutePath()));

            Map<String, String> vals = new HashMap<>();
            List<Serie> series = new ArrayList<>();

            // Process each line (time serie) and re-construct the Serie object
            lines.stream().forEach(line -> {
                String[] split = line.split(" ");
                for (int i = 2; i < split.length - 1; i = i + 2) {
                    vals.put(split[i], split[i + 1]);
                }
                String[] productName_serieName = split[1].split(":");
                series.add(new Serie(productName_serieName[0], productName_serieName[1], vals));
            });
            f.delete();
            // Laaunch mass insert series using pipelining with List of Series
            return repo.massInsert(series);
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage());
            throw new ErrorException(e.getMessage());
        }
    }
}
