package weoptim.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weoptim.configuration.Params;
import weoptim.domain.Serie;
import weoptim.service.OperationService;
import weoptim.service.SerieService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 *This Service provide methods for time series computations ( sum,difference,mult,division and shift)
 *
 */
@Service
public class OperationServiceImpl implements OperationService {

    //Wiring an instance of the SerieService to get series to calculate
    @Autowired
    SerieService serieService;

    //Wiring an instance of Params ( check application.yaml) to get the Precision of the output Result
    @Autowired
    Params params;

    /**
     * Method to sum two time series
     * @return Map of calculated values from the input series
     * @param serie1 : First Operand
     * @param serie2 : Second Operand
     **/
    @Override
    public Map<String,String> sum(Serie serie1, Serie serie2) {
        Map<String,String> results = new HashMap<>();
        serie1.getValues().forEach((period1,value1) -> {
            String value2 = serie2.getValues().get(period1);
            if(value2 != null ) results.put(period1,String.valueOf(Double.valueOf(value1)+Double.valueOf(value2)));
            else results.put(period1,value1);
        });
        serie2.getValues().forEach((period2,value2) -> {
            String value1 = serie1.getValues().get(period2);
            if(value1 == null ) results.put(period2,value2);
        });
        return results;
    }

    /**
     * Method to calculate difference between two time series
     * @return Map of calculated values from the input series
     * @param serie1 : First Operand
     * @param serie2 : Second Operand
     **/
    @Override
    public Map<String,String> difference(Serie serie1, Serie serie2) {
        Map<String,String> results = new HashMap<>();
        // Calculate for serie1 periods
        serie1.getValues().forEach((period1,value1) -> {
            String value2 = serie2.getValues().get(period1);
            if(value2 != null ) results.put(period1,String.valueOf(Double.valueOf(value1)-Double.valueOf(value2)));
            else results.put(period1,value1);
        });
        //calculate for serie2
        serie2.getValues().forEach((period,value) -> {
            String value1 = serie1.getValues().get(period);
            double val = - Double.valueOf(value);
            if(value1 == null ) results.put(period,String.valueOf(val));
        });
        return results;

    }

    /**
     * Method to calculate multiplication between two time series
     * @return Map of calculated values from the input series
     * @param serie1 : First Operand
     * @param serie2 : Second Operand
     **/
    @Override
    public Map<String,String> multiplication(Serie serie1, Serie serie2) {
        Map<String,String> results = new HashMap<>();
        // calculate for serie1 periods
        serie1.getValues().forEach((period,value) -> {
            String value2 = serie2.getValues().get(period);
            BigDecimal val = new BigDecimal(value);
            if(value2 != null ) {
                BigDecimal val2 = new BigDecimal(value2);
                results.put(period,String.valueOf(val.multiply(val2)));
            }
            else results.put(period,"0");
        });

        serie2.getValues().forEach((period,value) -> {
            String value1 = serie1.getValues().get(period);
            if(value1 == null ) results.put(period,"0");
        });

        return results;
    }

    /**
     * Method to calculate division between two time series
     * @return Map of calculated values from the input series
     * @param serie1 : First Operand
     * @param serie2 : Second Operand
     **/
    @Override
    public Map<String,String> division(Serie serie1, Serie serie2) {
        Map<String,String> results = new HashMap<>();
        serie1.getValues().forEach((period,value) -> {
            String value2 = serie2.getValues().get(period);
            BigDecimal val = new BigDecimal(value);
            if(value2 != null) {
                BigDecimal val2 = new BigDecimal(value2);
                int descriminant = val2.intValue();
                if(descriminant != 0) results.put(period,val.divide(val2,params.getPrecision(), RoundingMode.HALF_UP)+"");
            }
        });
        return results;

    }

    /**
     * Method to  shift a time serie with number of steps
     * @return Map of calculated values from the input series
     * @param serie : Serie Object to shift
     * @param steps : number of periods to shift serie. if positive value the  serie is shifted tothe Left, otherwise the shift goes right.
     **/
    @Override
    public Map<String,String> decalage(Serie serie, int steps) {
        Map<String,String> results = new HashMap<>();
        List<String> periods = new ArrayList<>();
        List<String> values = new ArrayList<>();
        // split the input serie between two lists : List of periods and list of values
        serie.getValues().forEach((period,value) -> {
            periods.add(period);
            values.add(value);
        });

        if(steps >= 0) {
            for (int i = 0; i < periods.size(); i++) {
                if (i + steps < periods.size()) {
                    String value = values.get(i + steps);
                    if (value != null) results.put(periods.get(i), value);
                }
            }
        } else {
            for (int i = -steps; i < periods.size(); i++) {
                    String period = periods.get(i);
                    String value = values.get(i+steps);
                    if (value != null) results.put(period, value);
            }
        }
        return results;
    }

    /**
     * Method to sum two time series
     * @return Map of calculated values from the input series
     * @param idProduct1 : ID Product of the First Operand
     * @param iDserie1 : Name of the serie of the First Operand
     * @param idProduct2 : ID Product of the Second Operand
     * @param iDserie2 : Name of the serie of the Second Operand
     **/
    @Override
    public Map<String,String> sum(String iDserie1, String idProduct1, String iDserie2, String idProduct2) {
        Serie serie = serieService.findOne(idProduct1, iDserie1);
        Serie serie2 = serieService.findOne(idProduct2, iDserie2);
        return sum(serie,serie2);
    }

    /**
     * Method to calculate the difference between two time series
     * @return Map of calculated values from the input series
     * @param idProduct1 : ID Product of the First Operand
     * @param iDserie1 : Name of the serie of the First Operand
     * @param idProduct2 : ID Product of the Second Operand
     * @param iDserie2 : Name of the serie of the Second Operand
     **/
    @Override
    public Map<String,String> difference(String iDserie1, String idProduct1, String iDserie2, String idProduct2) {
        Serie serie = serieService.findOne(idProduct1, iDserie1);
        Serie serie2 = serieService.findOne(idProduct2, iDserie2);
        return difference(serie,serie2);
    }

    /**
     * Method to do multiplication of two time series
     * @return Map of calculated values from the input series
     * @param idProduct1 : ID Product of the First Operand
     * @param iDserie1 : Name of the serie of the First Operand
     * @param idProduct2 : ID Product of the Second Operand
     * @param iDserie2 : Name of the serie of the Second Operand
     **/
    @Override
    public Map<String,String> multiplication(String iDserie1, String idProduct1, String iDserie2, String idProduct2) {
        Serie serie = serieService.findOne(idProduct1, iDserie1);
        Serie serie2 = serieService.findOne(idProduct2, iDserie2);
        return multiplication(serie,serie2);
    }

    /**
     * Method to do a division between two time series
     * @return Map of calculated values from the input series
     * @param idProduct1 : ID Product of the First Operand
     * @param iDserie1 : Name of the serie of the First Operand
     * @param idProduct2 : ID Product of the Second Operand
     * @param iDserie2 : Name of the serie of the Second Operand
     **/
    @Override
    public Map<String,String> division(String iDserie1, String idProduct1, String iDserie2, String idProduct2) {
        Serie serie = serieService.findOne(idProduct1, iDserie1);
        Serie serie2 = serieService.findOne(idProduct2, iDserie2);
        return division(serie,serie2);
    }

    /**
     * Method to shift a time serie with a number of steps
     * @return Map of calculated values from the input series
     * @param idProduct1 : ID Product of the First Operand
     * @param iDserie1 : Name of the serie of the First Operand
     * @param steps : number of periods to shit the input serie by.
     **/
    @Override
    public Map<String,String> decalage(String iDserie1, String idProduct1, int steps) {
        Serie serie = serieService.findOne(idProduct1, iDserie1);
        return decalage(serie,steps);
    }
}
