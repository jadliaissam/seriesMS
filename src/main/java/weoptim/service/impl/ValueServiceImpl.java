package weoptim.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weoptim.exception.ForbidenException;
import weoptim.exception.NotFoundException;
import weoptim.repository.ValueRepository;
import weoptim.service.ValueService;

import java.util.List;
import java.util.Map;

/*
 *This Service provide methods for values (of time series) management using ValueRepository
 */
@Service
public class ValueServiceImpl implements ValueService {

    //Wiring an instance of the ValueRepository
    @Autowired
    ValueRepository repo;

    /**
     * Method to get ONE value
     * @return The Value (String)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param period : The chosen period
     **/
    @Override
    public String findOne(String idProduct, String serieName, String period) {
        if(! repo.exists(idProduct,serieName,period))
            throw new NotFoundException("This Period (ID : "+period+". Serie : "+serieName+". Product #"+idProduct+") does not exist.");
        return repo.findValue(idProduct,serieName,period);
    }

    /**
     * Method to get Multiple values
     * @return Map of selected values (key=period,value=Value)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param periods : List of chosen periods
     **/
    @Override
    public Map<String, String> findValues(String idProduct, String serieName, List<String> periods) {
        return repo.findValues(idProduct,serieName,periods);
    }

    /**
     * Method to save ONE value
     * @return The Saved Value (String)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param period : The chosen period
     * @param value : The value to save
     **/
    @Override
    public String save(String idProduct, String serieName, String period, String value) {
        if(repo.exists(idProduct,serieName,period))
            throw new ForbidenException("This Key (ID : "+period+") Exists Already ! remove it First or use Update Route");
        return   repo.saveValue(idProduct,serieName,period,value);
    }

    /**
     * Method to update ONE value. it calls the saveValue() method since Redis does not have a specific update method.
     * @return The Updated Value (String)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param period : The chosen period
     * @param value : The value to update
     **/
    @Override
    public String update(String idProduct, String serieName, String period, String value) {
        if(! repo.exists(idProduct,serieName,period))
            throw new NotFoundException("The Period (ID:"+period+". Serie:"+serieName+". Product #"+idProduct+") does not exist.");
        return repo.updateValue(idProduct,serieName,period,value);
    }

    /**
     * Method to remove ONE value
     * @return The Removed Value (String)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param period : The chosen period
     **/
    @Override
    public boolean remove(String idProduct, String serieName, String period) {
        if(! repo.exists(idProduct,serieName,period))
            throw new NotFoundException("This Key (ID:"+period+". Serie : "+serieName+". Product #"+idProduct+") does not exist.");
        return repo.removeValue(idProduct,serieName,period);
    }

    /**
     * Method to remove Multiple values
     * @return operation success status (true/false)
     * @param idProduct : Product ID
     * @param serieName : Time serie Name
     * @param periods : List of chosen periods to remove with their values
     **/
    @Override
    public boolean removeValues(String idProduct, String serieName, List<String> periods) {
        return repo.removeValues(idProduct,serieName,periods);
    }
}
