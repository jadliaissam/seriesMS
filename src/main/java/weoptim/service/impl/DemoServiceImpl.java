package weoptim.service.impl;

import org.springframework.stereotype.Service;
import weoptim.exception.ErrorException;
import weoptim.repository.SerieRepository;

import java.io.FileWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class DemoServiceImpl {

    static final Logger logger = Logger.getLogger(SerieRepository.class.getName());


    public String generateDataFile(int start, int end) {
        Random rand = new Random();
        Map<String, String> commandes = new HashMap<>();
        for (int i = 0; i < 400; i++) {
            commandes.put("" + i, "" + rand.nextInt(100));
        }
        Map<String, String> ventes = new HashMap<>();
        for (int i = 0; i < 400; i++) {
            ventes.put("" + i, "" + rand.nextInt(100));
        }

        Map<String, String> stock = new HashMap<>();
        for (int i = 0; i < 400; i++) {
            stock.put("" + i, "" + rand.nextInt(100));
        }

        Map<String, String> retours = new HashMap<>();
        for (int i = 0; i < 400; i++) {
            retours.put("" + i, "" + rand.nextInt(100));
        }

        List<String> titles = new ArrayList<>();
        titles.add("ventes");
        titles.add("commandes");
        titles.add("stock");
        titles.add("retours");

        List<Map<String, String>> series = new ArrayList<>();
        series.add(ventes);
        series.add(commandes);
        series.add(stock);
        series.add(retours);

        int size = end - start;
        try (FileWriter fileWriter = new FileWriter("demo-db-" + size)) {
            Date deb = new Date();
            for (long k = start; k < end; k++) {
                for (int i = 0; i < series.size(); i++)
                    addLine(fileWriter,series.get(i),String.valueOf(k), titles.get(i));
            }
            long time = new Date().getTime() - deb.getTime();
            return "Execution Time (ms) : " + time;
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            throw new ErrorException(e.getMessage());
        }
    }

    public void addLine(FileWriter fr, Map<String,String> serie, String idProduct, String serieName){
        try {
            AtomicReference<String> str = new AtomicReference<>("HMSET " + idProduct + ":" + serieName);
            serie.forEach((String period, String val) -> str.set(str + " " + period + " " + val));
            str.set(str + "\r\n");
            fr.append(str.get());
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

}
