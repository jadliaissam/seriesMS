package weoptim.service;

import org.springframework.web.multipart.MultipartFile;
import weoptim.domain.Serie;

import java.util.List;
import java.util.Map;

public interface SerieService {
    public Serie findOne(String idProduct, String serie);
    public Map findByseriesForProductListBySerieName(List<String> ids, String serie);
    public List<Serie> saveListSeries(String idProduct, List<Serie> series);
    public Serie save(Serie serie);
    public Serie update(Serie serie);
    public Serie updateNew(Serie serie);
    public boolean remove(String idProduct, String serie);
    public int importFile(MultipartFile file);
}
